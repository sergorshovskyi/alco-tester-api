import time

from locust import HttpUser, between, task


class WebsiteUser(HttpUser):
    wait_time = between(5, 15)

    @task
    def post_student(self):
        self.client.post("coef/", json={
  "affect": "hard",
  "amount": 50,
  "coef": 1.2
})

    @task
    def get_student(self):
        self.client.get("coef/")
