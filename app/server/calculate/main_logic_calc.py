from ..helpers.coef_extract import *


async def get_result(alco, amount, age, gender, experience, meal):
    age_coef = get_year_coef(age)
    alco_amount_coef = await get_amount_coef(amount, alco)
    gender_coef = get_sex_coef(gender)
    experience_coef = get_experience_coef(experience)
    meal_coef = get_meal_coef(meal)
    return calculate_state(age_coef, alco_amount_coef, gender_coef, experience_coef, meal_coef)


def calculate_state(*args):
    default_coef = 1000
    for coef in args:
        default_coef*=coef
    return default_coef


def get_string_result(coef):
    if coef < 2500:
        return 'Все нормально'
    elif 5000 >= coef > 2500:
        return 'Обережно'
    elif coef > 5000:
        return 'Пора закінчувати'
    else:
        return ErrorResponseModel("An error occurred.", 404, "Coef is not supported: {0}".format(coef))



def get_how_long_to_drive_car(coef):
    if coef < 8000:
        return 'За кермо можна сідати приблизно через 3 години'
    elif 11000 >= coef > 8000:
        return 'За кермо можна сідати приблизно через 6 годин'
    elif 13000 >= coef > 11000:
        return 'За кермо можна сідати приблизно через 12 годин'
    elif coef > 13000:
        return 'Краще потерпіти одну сутку, і лиш тоді сідати за кермо'
    else:
        return ErrorResponseModel("An error occurred.", 404, "get_how_long_to_drive_car does not support current coef: {0}".format(coef))