import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config
MONGO_DETAILS = config('MONGO_DETAILS') # read environment variable.


# MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.alco

alcohol_collection = database.get_collection("alcohol_collection")
coef_collection = database.get_collection("coef_collection")

# alco region
def alco_helper(alco) -> dict:
    return {
        "id": str(alco["_id"]),
        "alco_name": alco["alco_name"],
        "affect": alco["affect"],
    }


async def retrieve_alcos():
    alcos = []
    async for alcho in alcohol_collection.find():
        alcos.append(alco_helper(alcho))
    return alcos


async def add_alco(alco_data: dict) -> dict:
    alco = await alcohol_collection.insert_one(alco_data)
    new_alco = await alcohol_collection.find_one({"_id": alco.inserted_id})
    return alco_helper(new_alco)


async def retrieve_alco(id: str) -> dict:
    alco = await alcohol_collection.find_one({"_id": ObjectId(id)})
    if alco:
        return alco_helper(alco)


async def update_alco(id: str, data: dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    alco = await alcohol_collection.find_one({"_id": ObjectId(id)})
    if alco:
        updated_alco = await alcohol_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_alco:
            return True
        return False


async def delete_alco(id: str):
    student = await alcohol_collection.find_one({"_id": ObjectId(id)})
    if student:
        await alcohol_collection.delete_one({"_id": ObjectId(id)})
        return True


# alco coef region
def alco_coef_helper(alco_coef) -> dict:
    return {
        "id": str(alco_coef["_id"]),
        "affect": alco_coef["affect"],
        "amount": alco_coef["amount"],
        "coef": alco_coef["coef"],
    }



async def retrieve_alco_coefs():
    coefs = []
    async for coef in coef_collection.find():
        coefs.append(alco_coef_helper(coef))
    return coefs


async def add_alco_coef(alco_coef_data: dict) -> dict:
    coef = await coef_collection.insert_one(alco_coef_data)
    new_coef = await coef_collection.find_one({"_id": coef.inserted_id})
    return alco_coef_helper(new_coef)


async def retrieve_coef(id: str) -> dict:
    coef = await coef_collection.find_one({"_id": ObjectId(id)})
    if coef:
        return alco_coef_helper(coef)


async def update_coef(id: str, data: dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    coef = await coef_collection.find_one({"_id": ObjectId(id)})
    if coef:
        updated_coef = await coef_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_coef:
            return True
        return False


async def delete_coef(id: str):
    coef = await coef_collection.find_one({"_id": ObjectId(id)})
    if coef:
        await coef_collection.delete_one({"_id": ObjectId(id)})
        return True
