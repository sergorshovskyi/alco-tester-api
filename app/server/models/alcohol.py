from typing import Optional

from pydantic import BaseModel, Field


class AlcoholSchema(BaseModel):
    alco_name: str = Field(...)
    affect: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "alco_name": "vodka",
                "affect": "hard",
            }
        }


class UpdateAlcoholModel(BaseModel):
    alco_name: Optional[str]
    affect: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "alco_name": "vodka",
                "affect": "hard",
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}