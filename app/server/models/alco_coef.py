from typing import Optional

from pydantic import BaseModel, Field


class AlcoCoefSchema(BaseModel):
    affect: str = Field(...)
    amount: int = Field(..., gt=0, lt=5000)
    coef: float = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "affect": "hard",
                "amount": 50,
                "coef": 1.2,
            }
        }


class UpdateAlcoCoefModel(BaseModel):
    affect: Optional[str]
    amount: Optional[int]
    coef: Optional[float]

    class Config:
        schema_extra = {
            "example": {
                "affect": "hard",
                "amount": 50,
                "coef": 1.2,
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}