# years
import enum

YEARS_LESS_20 = 1.5
YEARS_LESS_35 = 1.4
YEARS_LESS_55 = 1.3
YEARS_LESS_90 = 1.6

# sex
MALE = 1.8
FEMALE = 2

# # alcohol type
# EASY_ALC = 1.5
# MEDIUM_ALC = 1.7
# HARD_ALC = 2

#
# class EasyConsumed(enum.Enum):
#     less_500 = 1.2
#     less_1000 = 1.4
#     less_3000 = 1.7
#     less_5000 = 2
#
#
# class MediumConsumed(enum.Enum):
#     less_400 = 1.2
#     less_700 = 1.5
#     less_1000 = 1.6
#     less_1500 = 1.8
#     less_2000 = 2
#
#
# class HardConsumed(enum.Enum):
#     less_50 = 1.1
#     less_100 = 1.3
#     less_200 = 1.4
#     less_600 = 1.5
#     less_1000 = 2



# experience
ALMOST_EVERYDAY = 1.1
ONCE_A_WEEK = 1.2
SEVERAL_TIMES_A_MONTH = 1.3
SEVERAL_TIMES_A_YEAR = 1.5
ALMOST_NEVER = 1.7

# meals
ATE_WELL = 1.1
SNACK = 1.3
NOTHING = 1.5
