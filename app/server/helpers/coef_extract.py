from fastapi import HTTPException
from ..models.alco_coef import ErrorResponseModel
from ..database import retrieve_alcos, retrieve_alco_coefs
from .constants import *


def amount_helper(affect, amount):
    if affect == 'easy':
        if amount > 0 and amount < 500:
            return 500
        elif amount >= 500 and amount < 1000:
            return 1000
        elif amount >=1000 and amount < 3000:
            return 3000
        elif amount >=3000 and amount < 5000:
            return 5000
        else:
            return 1

    elif affect == 'medium':
        if amount > 0 and amount < 400:
            return 400
        elif amount >= 400 and amount < 700:
            return 700
        elif amount >=700 and amount < 1000:
            return 1000
        elif amount >=1000 and amount < 1500:
            return 1500
        elif amount >=1500 and amount < 2000:
            return 2000
        else:
            return 1
    elif affect == 'hard':
        if amount > 0 and amount < 50:
            return 50
        elif amount >= 50 and amount < 100:
            return 100
        elif amount >=100 and amount < 200:
            return 200
        elif amount >=200 and amount < 600:
            return 600
        elif amount >=600 and amount < 1000:
            return 1000
        else:
            return 1


def get_year_coef(year):
    if year < 16:
        return 1
    elif year > 90:
        return 1
    elif 20 >= year > 16:
        return YEARS_LESS_20
    elif 35 >= year > 20:
        return YEARS_LESS_35
    elif 55 >= year > 35:
        return YEARS_LESS_55
    elif 90 >= year > 55:
        return YEARS_LESS_90
    else:
        return 1

def get_sex_coef(sex):
    if sex == '1':
        return FEMALE
    elif sex == '2':
        return MALE
    else:
        return 1

def get_experience_coef(experience):
    if experience == 1:
        return ALMOST_EVERYDAY
    elif experience == 2:
        return ONCE_A_WEEK
    elif experience == 3:
        return SEVERAL_TIMES_A_MONTH
    elif experience == 4:
        return SEVERAL_TIMES_A_YEAR
    elif experience == 5:
        return ALMOST_NEVER
    else:
        return 1


def get_meal_coef(meal):
    if meal == 1:
        return ATE_WELL
    elif meal == 2:
        return SNACK
    elif meal == 3:
        return NOTHING
    else:
        return 1



async def get_amount_coef(amount, alco):
    alcos = await retrieve_alcos()
    affect = None
    alco_name = [a['alco_name'] for a in alcos]
    if alco not in alco_name:
        raise HTTPException(status_code=404, detail="Alco name was not found in alco list.")
    else:
        for item in alcos:
            if item["alco_name"] == alco:
                affect = item["affect"]
                break
    coefs = await retrieve_alco_coefs()
    coef = None
    amount_for_coef_table = amount_helper(affect, amount)
    for item in coefs:
        if item["amount"] == amount_for_coef_table and item["affect"] == affect:
            coef = item["coef"]
            break
    if coef == None:
        raise HTTPException(status_code=404, detail="Coef was not found in coefficient list.")
    return coef



