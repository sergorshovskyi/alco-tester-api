from fastapi import FastAPI, Form

from .calculate.main_logic_calc import get_result, get_string_result, get_how_long_to_drive_car
from .routes.alcohol import router as AlcoRouter
from .routes.alco_coef import router as AlcoCoefRouter
app = FastAPI()

app.include_router(AlcoRouter, tags=["Alcohol"], prefix="/alco")
app.include_router(AlcoCoefRouter, tags=["Coef"], prefix="/coef")


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this fantastic app!"}

@app.post("/calculate/")
async def calculate(alco: str = Form(...), amount: int = Form(..., gt=0, lt=5000), age: int = Form(..., gt=15, lt=90),
                gender: int = Form(..., gt=0, lt=3), experience: int = Form(..., gt=0, lt=6), meal: int = Form(..., gt=0, lt=4)):
    coef = await get_result(alco, int(amount), int(age), int(gender), int(experience), int(meal))
    string_result = get_string_result(coef)
    drive_result = get_how_long_to_drive_car(coef)
    return {"coef": coef, "string": string_result, "when to drive": drive_result}

# if __name__=="__main__":
#     app.run()