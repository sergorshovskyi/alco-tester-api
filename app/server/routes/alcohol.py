from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from ..database import (
    add_alco,
    delete_alco,
    retrieve_alco,
    retrieve_alcos,
    update_alco,
)
from ..models.alcohol import (
    ErrorResponseModel,
    ResponseModel,
    AlcoholSchema,
    UpdateAlcoholModel,
)

router = APIRouter()

@router.post("/", response_description="Alco data added into the database")
async def add_alco_data(alco: AlcoholSchema = Body(...)):
    alco = jsonable_encoder(alco)
    new_alco = await add_alco(alco)
    return ResponseModel(new_alco, "Alco added successfully.")

@router.get("/", response_description="Alcos retrieved")
async def get_alcos():
    alcos = await retrieve_alcos()
    if alcos:
        return ResponseModel(alcos, "Alcos data retrieved successfully")
    return ResponseModel(alcos, "Empty list returned")


@router.get("/{id}", response_description="Alco data retrieved")
async def get_alco_data(id):
    alco = await retrieve_alco(id)
    if alco:
        return ResponseModel(alco, "Alco data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Alco doesn't exist.")

@router.put("/{id}")
async def update_alco_data(id: str, req: UpdateAlcoholModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_alco = await update_alco(id, req)
    if updated_alco:
        return ResponseModel(
            "Alco with ID: {} name update is successful".format(id),
            "Alco name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the alco data.",
    )


@router.delete("/{id}", response_description="Alco data deleted from the database")
async def delete_alco_data(id: str):
    deleted_alco = await delete_alco(id)
    if deleted_alco:
        return ResponseModel(
            "Alco with ID: {} removed".format(id), "Alco deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Alco with id {0} doesn't exist".format(id)
    )