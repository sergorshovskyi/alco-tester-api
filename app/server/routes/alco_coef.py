from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from ..database import (
    add_alco_coef,
    delete_coef,
    retrieve_alco_coefs,
    retrieve_coef,
    update_coef,
)
from ..models.alco_coef import (
    ErrorResponseModel,
    ResponseModel,
    AlcoCoefSchema,
    UpdateAlcoCoefModel,
)

router = APIRouter()

@router.post("/", response_description="Alco coef data added into the database")
async def add_alco_coef_data(coef: AlcoCoefSchema = Body(...)):
    coef = jsonable_encoder(coef)
    new_coef = await add_alco_coef(coef)
    return ResponseModel(new_coef, "Alco coef added successfully.")

@router.get("/", response_description="Alco coefs retrieved")
async def get_coefs():
    coefs = await retrieve_alco_coefs()
    if coefs:
        return ResponseModel(coefs, "Alco coefs data retrieved successfully")
    return ResponseModel(coefs, "Empty list returned")


@router.get("/{id}", response_description="Alco coef data retrieved")
async def get_coef_data(id):
    coef = await retrieve_coef(id)
    if coef:
        return ResponseModel(coef, "Alco coef data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Alco coef doesn't exist.")

@router.put("/{id}")
async def update_coef_data(id: str, req: UpdateAlcoCoefModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_coef = await update_coef(id, req)
    if updated_coef:
        return ResponseModel(
            "Alco coef with ID: {} name update is successful".format(id),
            "Alco coef name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the alco coef data.",
    )


@router.delete("/{id}", response_description="Alco coef data deleted from the database")
async def delete_coef_data(id: str):
    deleted_coef = await delete_coef(id)
    if deleted_coef:
        return ResponseModel(
            "Alco coef with ID: {} removed".format(id), "Alco coef deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Alco coef with id {0} doesn't exist".format(id)
    )