# import json
# # import unittest
#
# # import asynctest as asynctest
# import requests
# import pytest
# pytestmark = pytest.mark.asyncio
#
# from app.server.database import alcohol_collection
#
#
# # class TestAlcoEntity(asynctest.TestCase):
# #
# #     async def setUp(self):
# #         pass
# async def test_get_alco(self):
#         response_decoded_json = requests.get("http://localhost:8000/alco/")
#         self.assertEqual(200, response_decoded_json.status_code, "Get /alco/ return bad request error code")
#         resp = response_decoded_json.json()
#         self.assertIsNotNone(resp['data'][0][0]['alco_name'], "Get /alco/ return incorrect data, alco_name is null")
#         self.assertEqual('Alcos data retrieved successfully', resp['message'], "Get /alco/ return wrong message: {0}".format(resp['message']))
#
# async def test_add_alco(self):
#         response_decoded_json = requests.post("http://localhost:8000/alco/", data=json.dumps({"alco_name": "for_test", "affect": "hard"}))
#
#         self.assertEqual(200, response_decoded_json.status_code, "Post /alco/ return bad request error code")
#         resp = response_decoded_json.json()
#         self.assertEqual('Alco added successfully.', resp['message'], "Post /alco/ return wrong message: {0}".format(resp['message']))
#
# async def test_add_alco_negative(self):
#         alco = await alcohol_collection.insert_one({"alco_name": "12345", "affect": "222"})
#         # id = alco.inserted_id
#         # add_alc = await add_alco({"alco_name": "12345", "affect": "222"})
#         # id = add_alc['_id']
#         # print(id)
#         response_decoded_json = requests.post("http://localhost:8000/alco/", data=json.dumps({"alco_name": "for_test", "effect": "hard"}))
#
#         self.assertEqual(422,
#                          response_decoded_json.status_code,
#                          "Post /alco/ must return 422 error code in case of incorrect field, code is {0}".format(response_decoded_json.status_code))
#         resp = response_decoded_json.json()
#         self.assertEqual('field required', resp['detail'][0]['msg'], "Post /alco/ return wrong message: {0}".format(resp['detail'][0]['msg']))
#
#
#     # def test_login_admin(self):
#     #     response_decoded_json = requests.post(UrlAuth.url_login, data=json.dumps(AuthPayloads.payload_admin),
#     #                                           headers=Header.header)
#     #     resp = response_decoded_json.json()
#     #     print(resp["role"])
#     #     self.assertEqual("Admin", resp["role"], "You don't login with correct role")
#     #     self.assertEqual(200, response_decoded_json.status_code, "You have BAD REQUEST")
# #
# #     async def tearDown(self):
# #         pass
# #
# #
# # if __name__ == '__main__':
# #     unittest.main()
