AlcoTester (WIP)

[Документація 1.1](https://docs.google.com/document/d/1GzMr1vPV1ZsAisBA_tkev5MQV8AzIxpFLLOx_6HsfW8/edit?usp=sharing 
) - доступно для ЧНУ-аккаунтів

_calculate folder_ - для скриптів, що відповідають за логіку обрахунку по наданим коефіцієнтам

_models_ - сутності для бази даних

_routes_ - методи API

_constants_ - для незмінних даних, констант, файлів і тд.

_helpers_ - для будь-яких допоміжних ф-цій та методів(extract coeffs, constants)

_main.py_ - точка запуску програми

_database.py_ - методи для роботи з MongoDB

python app/main.py - запуск з консолі